/* import Layout from '../components/MyLayout';
import Link from 'next/link';

const PostLink = props => (
  <li>
    // <Link href={`/post?title=${props.title}`}>
    <Link href='/p/post/[id]' as={`/p/post/${props.id}`}>
      <a>{props.id}</a>
    </Link>
  </li>
);

export default function Index() {
    return (
      <Layout>
        <h1>My Blog</h1>
        <ul>
          <PostLink id="hello-nextjs" />
          <PostLink id="learn-nextjs" />
          <PostLink id="deploy-nextjs" />
        </ul>
      </Layout>
    );
} */

import Layout from '../components/MyLayout';
import Link from 'next/link';

function getPosts() {
  return [
    { id: 'hello-nextjs', title: 'Hello Next.js' },
    { id: 'learn-nextjs', title: 'Learn Next.js is awesome' },
    { id: 'deploy-nextjs', title: 'Deploy apps with ZEIT' }
  ];
}

const PostLink = ({ post }) => (
  <li>
    <Link href="/p/post/[id]" as={`/p/post/${post.id}`}>
      <a>{post.title}</a>
    </Link>
    <style jsx>{`
        li {
          list-style: none;
          margin: 5px 0;
        }

        a {
          text-decoration: none;
          color: green;
        }

        a:hover {
          opacity: 0.6;
        }
      `}</style>
  </li>
);

export default function Blog() {
  return (
    <Layout>
      <h1>My Blog</h1>
      <ul>
        {getPosts().map(post => (
          // <li key={post.id}>
          //   <Link href="/p/post/[id]" as={`/p/post/${post.id}`}>
          //     <a>{post.title}</a>
          //   </Link>
          // </li>
          <PostLink post={post} key={post.id}/>
        ))}
      </ul>
      <Link href='/quoteApi'>
          <a>API example</a>
      </Link>
      <Link href='/callApi'>
          <a>Call API</a>
      </Link>
      <style jsx>{`
        h1,
        a {
          font-family: 'Times New Roman';
          margin-right: 15px;
        }

        ul {
          padding: 0;
        }
      `}</style>
    </Layout>
  );
}